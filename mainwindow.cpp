#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QProcess"
#include "QFileDialog"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //    connect(ui->menuOpen_File,SIGNAL(triggered(QAction*)),this,SLOT(openFile())) ;
    connect(ui->actionOpen,SIGNAL(triggered()),this,SLOT(openFileSlot())) ;

    connect(ui->doubleSpinBox,SIGNAL(valueChanged(double)),this,SLOT(plotSlot2()));
    connect(ui->doubleSpinBox_2,SIGNAL(valueChanged(double)),this,SLOT(plotSlot2()));

    connect(ui->horizontalSlider,SIGNAL(sliderMoved(int)),this,SLOT(plotSlot1()));
    connect(ui->horizontalSlider_2,SIGNAL(sliderMoved(int)),this,SLOT(plotSlot1()));

    connect(ui->actionSave_As,SIGNAL(triggered()),this,SLOT(Save()));
    connect(ui->actionExport_to_CSV,SIGNAL(triggered()),this,SLOT(ExportCSV()));


    //connect(ui->actionTo_Image,SIGNAL(triggered()),this,SLOT(batchConverImage()));
    connect(ui->actionBmp,SIGNAL(triggered()),this,SLOT(batchConvertImageBmp()));
    connect(ui->actionJpg,SIGNAL(triggered()),this,SLOT(batchConvertImageJpg()));
    connect(ui->actionPdf,SIGNAL(triggered()),this,SLOT(batchConvertImagePdf()));
    connect(ui->actionPng,SIGNAL(triggered()),this,SLOT(batchConvertImagePng()));
    connect(ui->actionTo_CSV_2,SIGNAL(triggered()),this,SLOT(batchConvertCSV()));
    ui->horizontalSlider->setMinimum(100);
    qDebug()<<"crash" ;
//    ui->doubleSpinBox->setMinimum(100);
    qDebug()<<"crash" ;
  //  ui->doubleSpinBox->setValue(100);
  //  ui->horizontalSlider->setValue(100);


//    QVector
    ui->menuOpen_File->addSeparator();
    openHist= new QAction(tr("Hist1.XML"),this);
    connect(openHist,SIGNAL(triggered()),this,SLOT(openFileSlot()));
    ui->menuOpen_File->addAction(openHist);
    qDebug() << openHist->text() ;



    //qDebug() << "linesRead" << linesRead;
    ui->plot->addGraph()    ;
    unitvMulti = 1 ;
    unitsMulti = 1 ;

}
void MainWindow::batchConvertCSV()
{
    //QString ext =".png";
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),"./",QFileDialog::ShowDirsOnly| QFileDialog::DontResolveSymlinks);

    QDir recoredDir(dir);
    QStringList allFiles = recoredDir.entryList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst);//(QDir::Filter::Files,QDir::SortFlag::NoSort)
    //qDebug() << allFiles;
    QString stemp ;
    //qDebug() << recoredDir.path();
    for(int i = 0 ; i < allFiles.size() ; i++)
    {

        stemp = allFiles[i] ;
        if(stemp.endsWith(".XML",Qt::CaseInsensitive))
        {
            //qDebug() << allFiles[i] ;
            stemp = recoredDir.path()+"/" + stemp;
            openFile(stemp ) ;
            stemp.remove(".XML",Qt::CaseInsensitive);
            stemp += ".CSV";

            qDebug() << stemp;
            saveCSV( stemp );
            //SaveImage(stemp);

        }
    }


}

void MainWindow::batchConvertImagePdf()
{
    QString ext = ".pdf";
    batchConvertImage(ext);
}

void MainWindow::batchConvertImagePng()
{
    QString ext = ".png";
    batchConvertImage(ext);
}

void MainWindow::batchConvertImageJpg()
{
    QString ext = ".jpg";
    batchConvertImage(ext);
}

void MainWindow::batchConvertImageBmp()
{
    QString ext = ".bmp";
    batchConvertImage(ext);
}

void MainWindow::batchConvertImage(QString ext)
{
    //    QString ext =".png";
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),"./",QFileDialog::ShowDirsOnly| QFileDialog::DontResolveSymlinks);

    QDir recoredDir(dir);
    QStringList allFiles = recoredDir.entryList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst);//(QDir::Filter::Files,QDir::SortFlag::NoSort)
    //qDebug() << allFiles;
    QString stemp , sdir;
    //    qDebug() << recoredDir.path();
    for(int i = 0 ; i < allFiles.size() ; i++)
    {

        stemp = allFiles[i] ;
        if(stemp.endsWith(".XML",Qt::CaseInsensitive))
        {
            //            qDebug() << allFiles[i] ;
            stemp = recoredDir.path()+"/" + stemp;
            openFile(stemp ) ;
            stemp.remove(".XML",Qt::CaseInsensitive);
            stemp += ext;

            qDebug() << stemp;

            SaveImage(stemp);

        }
    }


}

void MainWindow::saveCSV(QString path )
{
    QString fileName = path ;
    QFile file(fileName);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    QString stemp ,stemp2;
    stemp2 = "%1,%2\n" ;
    if(unitsMulti == 1000) stemp2 = stemp2.arg("X [mS]") ; else
        if(unitsMulti == 1000 *1000) stemp2 = stemp2.arg("X [uS]") ;else
            stemp2 = stemp2.arg("X [S]");
    if(unitvMulti == 1000) stemp2 = stemp2.arg("Y [mV]") ; else
        if(unitvMulti == 1000 *1000) stemp2 = stemp2.arg("Y [uV]") ;else
            stemp2 = stemp2.arg("Y [V]");
    //      unitvMulti ;
//    qDebug() << stemp2;
    out << stemp2 ;
    for(int i = 0 ; i < x.size(); i++)
    {
        stemp = "%1,%2\n";
        stemp = stemp.arg(x[i]).arg(y[i]);
        //         qDebug() << stemp;

        out << stemp ;
    }


}

void MainWindow::ExportCSV()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),"./graph.CSV","");
    saveCSV(fileName );
}


void MainWindow::Save()
{
    QString filters;
    filters = ".bmp (Bmp (*.bmp) , .jpg (Jpeg (*.jpg ,*.jpeg ) , .pdf (Pdf (*.pdf) , .ras (Ras (*.ras) , .png (Png (*.png)";
    filters = "Bmp (*.bmp) ;; Jpeg (*.jpg ,*.jpeg ) ;; Pdf (*.pdf) ;; Png (*.png)";

    //QFileDialog dialog(this);
    //dialog.setNameFilters(filters);
    //dialog.exec();

    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                    "./graph",filters);

    //    QString fileName = dialog.selectedFiles()[0];
    SaveImage(fileName);
    qDebug() << fileName;


}

void MainWindow::SaveImage(QString path )
{

    if(path.contains("bmp",Qt::CaseInsensitive))
        ui->plot->saveBmp(path);else

        if(path.contains("jpg",Qt::CaseInsensitive))
            ui->plot->saveJpg(path);else

            if(path.contains("pdf",Qt::CaseInsensitive))
                ui->plot->savePdf(path);else

                if(path.contains("png",Qt::CaseInsensitive))
                    ui->plot->savePng(path);else
                {
                    qDebug() << "no format specified , assuming BMP";
                    ui->plot->saveBmp(path);
                }

}

void MainWindow::resizePlot()
{
    double Windowsize ;
    //if()
    //   Windowsize = 1.0 * linesRead / (1.0*ui->horizontalSlider->value() /100);
    ////   qDebug() <<phut lik "Windowsize" << Windowsize <<"slider2 max " << linesRead- Windowsize ;
    //   ui->horizontalSlider_2->setMaximum(linesRead- Windowsize );
    ////   qDebug() << "range:" << ui->horizontalSlider_2->value() << "," << ui->horizontalSlider_2->value() + Windowsize ;
    //   ui->plot->xAxis->setRange(ui->horizontalSlider_2->value(),ui->horizontalSlider_2->value() + Windowsize);
    //   ui->plot->replot();

    Windowsize = 1.0 * x.last() / (1.0*ui->doubleSpinBox->value() /100);
    //   qDebug() <<"Windowsize" << Windowsize <<"slider2 max " << linesRead- Windowsize ;
    ui->horizontalSlider_2->setMaximum(1000 ); //100% max
    //   qDebug() << "range:" << 1.0*ui->horizontalSlider_2->value()/100*x.last() << "," << 1.0*ui->horizontalSlider_2->value()/100*x.last() + Windowsize ;
    ui->plot->xAxis->setRange(1.0*ui->doubleSpinBox_2->value()/100*x.last(),1.0*ui->doubleSpinBox_2->value()/100*x.last() + Windowsize);
    ui->plot->replot();


}

void MainWindow::plotSlot1()
{
    //        qDebug() << "signal1 received";
    if(ui->horizontalSlider->value() < 100)
        ui->horizontalSlider->setValue(100);
    ui->doubleSpinBox->setValue(ui->horizontalSlider->value());
    ui->doubleSpinBox_2->setValue(ui->horizontalSlider_2->value()/10);
    resizePlot();
    //    plot();
}

void MainWindow::plotSlot2()
{
    ui->horizontalSlider->setValue(ui->doubleSpinBox->value());
    ui->horizontalSlider_2->setValue(1.0*ui->doubleSpinBox_2->value()*10);
    if(ui->horizontalSlider->value() < 100)
        ui->horizontalSlider->setValue(100);
    //        qDebug() << "signal2 received";
    resizePlot();
    //    plot();
}

void MainWindow::openFileSlot()
{

    //    qDebug() << "signal received";
    //    QFile file("");

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),"",tr("Files (FILE*.XML)"));
    //    qDebug() << fileName ;

    //    QFile file("/home/password/dso201/DSO201grapher-build-desktop-Qt_4_8_3_in_PATH__System__Release/FILE016.XML");
    OpenHistory.append(fileName);
            while(OpenHistory.size()>=5)
            OpenHistory.remove(0);
    openFile(fileName);
//qDebug() << OpenHistory;
}

void MainWindow::openFile(QString path )
{
    x.clear();
    y.clear();

    QString fileName = path ;

    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "Error , Unable to open file " + file.fileName() ;

    }
    linesRead=0 ;
    min = 0 ;
    max = 0 ;

    QString line ;
    QString sval;

    double timerange = 1;
    while (!file.atEnd())
    {

        line = file.readLine();
        line.remove("\n");
        //        qDebug() << line;

        if(line.contains("<timeRange>")&&line.contains("</timeRange>"))
        {
            sval = line ;
            sval.remove("<timeRange>") ;
            sval.remove("</timeRange>") ;

            timerange = sval.toDouble();

            unitsMulti = 1 ;
            while ( unitsMulti * timerange < 1)
            {
                unitsMulti *=1000 ;
                //                qDebug() << "rescaling";
            }
            //            qDebug() << unitsMulti << timerange;

        }
        if(line.contains("<val>")&&line.contains("</val>"))
        {

            //if(linesRead<600)
            {
                linesRead++;
                sval = line;
                sval.remove("<val>");
                sval.remove("</val>");
                ival = sval.toFloat();
                if(linesRead==1)
                {
                    max = ival ;
                    min = ival ;
                }
                if(ival > max)
                    max = ival;
                if(ival < min)
                    min = ival;
                //            qDebug() << "x" << x[x.size()-1] << "Y" << y[y.size()-1] << timerange;

                x.append (unitsMulti *timerange * linesRead / 4098 );
                y.append(sval.toFloat());
            }

        }

    }
    unitvMulti = 1 ;
    if(abs(max) <=  0.8)
    {
        unitvMulti = 1000 ;
        if(qAbs(max) <= 0.00008)
            unitvMulti = 1000*1000 ;
        //        qDebug() << "unitvMulti" << unitvMulti << "abs(max)" << qAbs(max);
        for(int i =0 ; i < y.size() ; i++)
        {
            y[i] *=  unitvMulti ;
            //            qDebug() << y[i];
        }
    }
    file.close();
    plot();

    ui->horizontalSlider->setRange(0, 5000);
    //    ui->horizontalSlider_2->setRange(0, linesRead);refactor!!1

}

void MainWindow::plot()
{

    ui->plot->graph(0)->setData(x,y);

//    qDebug() << "unitsMulti" << unitsMulti << "unitvMulti" << unitvMulti ;
    if(unitsMulti == 1000)
        ui->plot->xAxis->setLabel("X [ms]");
    else
        if(unitsMulti == 1000*1000)
            ui->plot->xAxis->setLabel("X [us]");
        else
    ui->plot->xAxis->setLabel("X [s]");

    //    qDebug() << "unitsMulti" << unitsMulti;
    if(unitvMulti == 1000)
        ui->plot->yAxis->setLabel("Y [mV]");
    else
        if(unitvMulti == 1000*1000)
            ui->plot->yAxis->setLabel("Y [uV]");
        else
            ui->plot->yAxis->setLabel("Y [V]");

    // set axes ranges, so we see all data:
//    ui->plot->xAxis->setRange(0, linesRead);
    //    ui->horizontalSlider->setValue(100);
    //    ui->horizontalSlider_2->setValue(0);
//    ui->plot->xAxis->setRange(ui->doubleSpinBox_2->value() , ui->doubleSpinBox->value());
    //        qDebug() << "min*1.1"<< min*1.1 << "max*1.1" << max*1.1;
    double delta;
    delta = max - min ;
    ui->plot->yAxis->setRange(unitvMulti *( min - 0.1*delta), unitvMulti * (max + 0.1*delta));

    ui->plot->replot();
    resizePlot();

    //ui->plot->
}

MainWindow::~MainWindow()
{
    delete ui;
}
