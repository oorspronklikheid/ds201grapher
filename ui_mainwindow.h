/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.3.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen;
    QAction *actionSave_As;
    QAction *actionExport_to_CSV;
    QAction *actionTo_CSV;
    QAction *actionPng;
    QAction *actionPdf;
    QAction *actionJpg;
    QAction *actionBmp;
    QAction *actionTo_CSV_2;
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QSlider *horizontalSlider_2;
    QDoubleSpinBox *doubleSpinBox;
    QCustomPlot *plot;
    QLabel *label;
    QDoubleSpinBox *doubleSpinBox_2;
    QSlider *horizontalSlider;
    QMenuBar *menuBar;
    QMenu *menuOpen_File;
    QMenu *menuBatch;
    QMenu *menuTo_Image;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(739, 502);
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSave_As = new QAction(MainWindow);
        actionSave_As->setObjectName(QStringLiteral("actionSave_As"));
        actionExport_to_CSV = new QAction(MainWindow);
        actionExport_to_CSV->setObjectName(QStringLiteral("actionExport_to_CSV"));
        actionTo_CSV = new QAction(MainWindow);
        actionTo_CSV->setObjectName(QStringLiteral("actionTo_CSV"));
        actionPng = new QAction(MainWindow);
        actionPng->setObjectName(QStringLiteral("actionPng"));
        actionPdf = new QAction(MainWindow);
        actionPdf->setObjectName(QStringLiteral("actionPdf"));
        actionJpg = new QAction(MainWindow);
        actionJpg->setObjectName(QStringLiteral("actionJpg"));
        actionBmp = new QAction(MainWindow);
        actionBmp->setObjectName(QStringLiteral("actionBmp"));
        actionTo_CSV_2 = new QAction(MainWindow);
        actionTo_CSV_2->setObjectName(QStringLiteral("actionTo_CSV_2"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy);
        label_2->setMaximumSize(QSize(40, 16777215));

        gridLayout->addWidget(label_2, 0, 3, 1, 1);

        horizontalSlider_2 = new QSlider(centralWidget);
        horizontalSlider_2->setObjectName(QStringLiteral("horizontalSlider_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(horizontalSlider_2->sizePolicy().hasHeightForWidth());
        horizontalSlider_2->setSizePolicy(sizePolicy1);
        horizontalSlider_2->setSingleStep(0);
        horizontalSlider_2->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider_2, 0, 4, 1, 1);

        doubleSpinBox = new QDoubleSpinBox(centralWidget);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setMaximumSize(QSize(75, 16777215));
        doubleSpinBox->setDecimals(0);
        doubleSpinBox->setMaximum(100000);

        gridLayout->addWidget(doubleSpinBox, 0, 2, 1, 1);

        plot = new QCustomPlot(centralWidget);
        plot->setObjectName(QStringLiteral("plot"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
        sizePolicy2.setHorizontalStretch(1);
        sizePolicy2.setVerticalStretch(1);
        sizePolicy2.setHeightForWidth(plot->sizePolicy().hasHeightForWidth());
        plot->setSizePolicy(sizePolicy2);
        plot->setMaximumSize(QSize(16777215, 16777215));

        gridLayout->addWidget(plot, 1, 0, 1, 6);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setMaximumSize(QSize(50, 16777215));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        doubleSpinBox_2 = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_2->setObjectName(QStringLiteral("doubleSpinBox_2"));
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(doubleSpinBox_2->sizePolicy().hasHeightForWidth());
        doubleSpinBox_2->setSizePolicy(sizePolicy3);
        doubleSpinBox_2->setMinimumSize(QSize(0, 0));
        doubleSpinBox_2->setMaximumSize(QSize(75, 16777215));
        doubleSpinBox_2->setDecimals(1);
        doubleSpinBox_2->setMinimum(0);
        doubleSpinBox_2->setMaximum(1e+06);
        doubleSpinBox_2->setSingleStep(0.1);

        gridLayout->addWidget(doubleSpinBox_2, 0, 5, 1, 1);

        horizontalSlider = new QSlider(centralWidget);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider, 0, 1, 1, 1);

        gridLayout->setRowStretch(0, 1);
        gridLayout->setColumnStretch(0, 1);

        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 739, 25));
        menuOpen_File = new QMenu(menuBar);
        menuOpen_File->setObjectName(QStringLiteral("menuOpen_File"));
        menuBatch = new QMenu(menuBar);
        menuBatch->setObjectName(QStringLiteral("menuBatch"));
        menuTo_Image = new QMenu(menuBatch);
        menuTo_Image->setObjectName(QStringLiteral("menuTo_Image"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuOpen_File->menuAction());
        menuBar->addAction(menuBatch->menuAction());
        menuOpen_File->addAction(actionOpen);
        menuOpen_File->addSeparator();
        menuOpen_File->addAction(actionSave_As);
        menuOpen_File->addAction(actionExport_to_CSV);
        menuBatch->addAction(menuTo_Image->menuAction());
        menuBatch->addAction(actionTo_CSV_2);
        menuTo_Image->addAction(actionBmp);
        menuTo_Image->addAction(actionJpg);
        menuTo_Image->addAction(actionPdf);
        menuTo_Image->addAction(actionPng);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionOpen->setText(QApplication::translate("MainWindow", "Open", 0));
        actionOpen->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", 0));
        actionSave_As->setText(QApplication::translate("MainWindow", "Save As..", 0));
        actionSave_As->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", 0));
        actionExport_to_CSV->setText(QApplication::translate("MainWindow", "Export to CSV", 0));
        actionExport_to_CSV->setShortcut(QApplication::translate("MainWindow", "Ctrl+E", 0));
        actionTo_CSV->setText(QApplication::translate("MainWindow", "To CSV", 0));
        actionPng->setText(QApplication::translate("MainWindow", "png", 0));
        actionPdf->setText(QApplication::translate("MainWindow", "pdf", 0));
        actionJpg->setText(QApplication::translate("MainWindow", "jpg", 0));
        actionBmp->setText(QApplication::translate("MainWindow", "bmp", 0));
        actionTo_CSV_2->setText(QApplication::translate("MainWindow", "To CSV", 0));
        label_2->setText(QApplication::translate("MainWindow", "Offset", 0));
        label->setText(QApplication::translate("MainWindow", "Zoom %", 0));
        menuOpen_File->setTitle(QApplication::translate("MainWindow", "File", 0));
        menuBatch->setTitle(QApplication::translate("MainWindow", "Batch", 0));
        menuTo_Image->setTitle(QApplication::translate("MainWindow", "To Image", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
