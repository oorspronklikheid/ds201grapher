#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    void plot();
    void resizePlot();


    double min ,max , ival ;
    QVector<double> x, y ;
    int linesRead ;
    long int unitsMulti ;
    long int unitvMulti ;
    QAction *openHist;
    QVector<QString> OpenHistory;


    void batchConvertImage(QString ext);
    void SaveImage(QString path ); //saves to path
    void openFile(QString path );
    void saveCSV(QString path );
public slots:
    void openFileSlot();
    void plotSlot1();
    void plotSlot2();

    void Save();    //gets user data
    void ExportCSV();
    void batchConvertCSV();
    void batchConvertImagePdf();
    void batchConvertImagePng();
    void batchConvertImageJpg();
    void batchConvertImageBmp();

};

#endif // MAINWINDOW_H
